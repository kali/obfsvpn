module 0xacab.org/leap/obfsvpn

go 1.17

require (
	git.torproject.org/pluggable-transports/goptlib.git v1.0.0
	gitlab.com/yawning/obfs4.git v0.0.0-20220204003609-77af0cba934d
)

require (
	filippo.io/edwards25519 v1.0.0-rc.1.0.20210721174708-390f27c3be20 // indirect
	github.com/dchest/siphash v1.2.1 // indirect
	gitlab.com/yawning/edwards25519-extra.git v0.0.0-20211229043746-2f91fcc9fbdb // indirect
	golang.org/x/crypto v0.0.0-20210711020723-a769d52b0f97 // indirect
	golang.org/x/sys v0.0.0-20210615035016-665e8c7367d1 // indirect
)
