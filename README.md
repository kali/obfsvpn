# ObfsVPN

The `obfsvpn` module contains a Go package that provides an easy mechanism to
establish and listen for network connections that use the ntor handshake and
OBFS4 obfuscation protocol.
